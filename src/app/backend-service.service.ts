import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Hero} from './objects/Hero';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class BackendService {

  baseUrl = environment.BACKEND_URL;
  constructor(
    private http: HttpClient
  ) { }

  saveHero = (hero: Hero) => {
    return this.http.post(`${this.baseUrl}/heroes`, hero, httpOptions);
  }

  getHeroes = () => {
    return this.http.get<Hero[]>(`${this.baseUrl}/heroes`);
  }
}

