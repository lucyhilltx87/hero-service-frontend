import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {NewHeroComponent} from './new-hero/new-hero.component';

const appRoutes: Routes = [
  { path: '', component: IndexComponent},
  { path: 'new-hero', component: NewHeroComponent}
];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
