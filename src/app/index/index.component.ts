import { Component, OnInit } from '@angular/core';
import {HeroesService} from '../heroes-service.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(
    public heroService: HeroesService
  ) { }

  ngOnInit() {
    this.heroService.getAllHeroes();
  }

}
