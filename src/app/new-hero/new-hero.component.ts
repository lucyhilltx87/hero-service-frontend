import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HeroesService} from '../heroes-service.service';
import { Hero } from '../objects/Hero';

@Component({
  selector: 'app-new-hero',
  templateUrl: './new-hero.component.html',
  styleUrls: ['./new-hero.component.css']
})
export class NewHeroComponent implements OnInit {

  hero: Hero = new Hero();

  constructor(
    private heroService: HeroesService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  addHero() {
    this.heroService.saveHero(this.hero);
    this.router.navigate(['/']);
  }

}
