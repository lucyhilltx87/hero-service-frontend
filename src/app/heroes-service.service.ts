import { Injectable } from '@angular/core';
import {BackendService} from './backend-service.service';
import {Hero} from './objects/Hero';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  heroes: Hero[];
  constructor(
    private backend: BackendService
  ) { }

  getAllHeroes = () => {
    this.backend.getHeroes()
      .subscribe((data: Hero[]) => this.heroes = data);
  }

  saveHero = (hero: Hero) => {
    this.backend.saveHero(hero)
      .subscribe(() => this.heroes.push(hero));
  }
}
