
export class Hero {
  public id: number;
  public name: string;
  public job: string;
  public home: string;
  public info: string;

}
